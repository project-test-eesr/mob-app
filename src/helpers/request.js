import axios from "axios";

const urlLocalServer = 'http://localhost:3010/'

const instance = axios.create({
    baseURL: urlLocalServer
})

export const userApi = {
    getAllUsers: () => {
        return instance.get(`users`).then(({
            data
        }) => data)
    },
    getUserById: (id) => {
        return instance.get(`users/${id}`).then(({
            data
        }) => data)
    },
    updateUser: (id, data) => {
        return instance.put(`users/${id}`, data).then(({ data }) => data)
    }
}