import { createStore } from "redux";
import { userAction } from "./reducers";
import { loginUser, logoutUser, updateUser } from "./actions";

const store = createStore(userAction)

export default store