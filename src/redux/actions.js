// Actions

export const userActions = {
    UPDATE: 'UPDATE'
}

export const sesionActions = {
    LOGIN: 'LOGIN',
    LOGOUT: 'LOGOUT'
}

export function loginUser(data) {
    return {
        type: sesionActions.LOGIN,
        ...data
    }
}

export function logoutUser() {
    return {
        type: sesionActions.LOGOUT
    }
}

export function updateUser(userData) {
    return {
        type: userActions.UPDATE,
        userData
    }
}