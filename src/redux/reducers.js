// Reducers

import { userActions, sesionActions } from "./actions";

const INITIALSTATE = { sessionState: sesionActions.LOGOUT }

export const userAction = (state = INITIALSTATE, action) => {
    switch (action.type) {
        case sesionActions.LOGIN:
            return { userData: action.userData, sessionState: sesionActions.LOGIN }

        case sesionActions.LOGOUT:
            return INITIALSTATE

        case userActions.UPDATE:
            return { ...state, userData: action.userData }

        default:
            return state
    }

}

// {
//     sessionState: 'LOGIN' - 'LOGOUT'
//     userData: {
//         name: 'a',
//         email: 'as'
//     },
//     userId
// }