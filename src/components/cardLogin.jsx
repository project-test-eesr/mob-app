// Card Component

import FormComponent from "./formComponent";

export default (props) => {
    return (
        <div className="card-login mt-3">
            <h1>Login</h1>
            <FormComponent />
        </div>
    )
}