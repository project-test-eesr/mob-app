// Form Component

import { Form, Input, Button, message } from 'antd';

import { useState } from "react";
import { connect } from "react-redux";

import { loginUser, sesionActions } from "../redux/actions";
import { userApi } from "../helpers/request";

const defaultFields = [
    { name: 'email', label: 'Email' },
    { name: 'password', label: 'Password' },
    { name: 'login', label: 'Ingresar', type: 'button' }
]

const validateMessages = {
    required: "${label} es requerido!",
    types: {
        email: "No es un email valido!",
    },
};

const FormComponent = ({ fields = defaultFields, onClickButton, initialValues, login }) => {
    const [fieldsForm, setFieldsForm] = useState(fields);
    const [btnLoading, setBtnLoading] = useState(false);
    const [form] = Form.useForm();

    const getData = async () => {
        return await userApi.getAllUsers()
    }

    const onFinish = async (values) => {
        setBtnLoading(true)
        let { email, password } = values
        let users = await getData()
        let loggedUser = users.find(item => item.email == email && item.password == password)
        if (loggedUser) login({ sessionState: sesionActions.LOGIN, userData: loggedUser })
        if (!loggedUser) {
            setBtnLoading(false)
            message.warning('Por favor verifica los datos ingresados')
        }
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Form
            id='customForm'
            layout='vertical'
            form={form}
            name='custom_form'
            validateMessages={validateMessages}
            onFinish={onClickButton || onFinish}
            onFinishFailed={onFinishFailed}
            initialValues={initialValues}
        >
            {fieldsForm.length > 0 && fieldsForm.map(field => (
                field.type == 'button' ?
                    <Form.Item key={`field_${field.name}`} >
                        <Button type="primary" htmlType="submit" loading={btnLoading}> {field.label} </Button>
                    </Form.Item> :
                    <Form.Item
                        key={`field_${field.name}`}
                        name={field.name}
                        label={field.label}
                        rules={[
                            { required: true },
                            { type: field.name == 'email' && 'email' },
                        ]}
                    >
                        {field.name == 'password' ? <Input.Password /> : <Input autoComplete="off" disabled={field.disabled} />}
                    </Form.Item>
            ))}

        </Form>
    )
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (data) => {
            dispatch(loginUser(data))
        }
    }
}

export default connect(null, mapDispatchToProps)(FormComponent)