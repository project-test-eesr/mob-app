// Home Component

import { useState, Fragment, useEffect } from "react";
import { connect } from "react-redux";

import FormComponent from "./formComponent";
import CardLogin from "./cardLogin";

import { Row, Col, Descriptions, Button, Modal, message, Avatar, Space } from "antd";

import { logoutUser, updateUser } from "../redux/actions";
import { userApi } from "../helpers/request";

const userFields = [
    { name: 'name', label: 'Nombre' },
    { name: 'email', label: 'Email', disabled: true },
    { name: 'gender', label: 'Género', disabled: true },
    { name: 'password', label: 'Password' },
    { name: 'phone', label: 'Celular' },
    { name: 'country', label: 'País' },
]

const HomeComponent = ({ userData, logout, updateData }) => {

    const [visible, setVisible] = useState(false);
    const [confirmLoading, setConfirmLoading] = useState(false);
    const [btnLoading, setBtnLoading] = useState(false);
    const [avatarColor, setAvatarColor] = useState('red');

    const showModal = () => {
        setVisible(true);
    };

    const handleCancel = () => {
        setVisible(false);
    };

    const updateUser = async (values) => {
        setConfirmLoading(true);
        let response = await userApi.updateUser(userData.id, values)
        setTimeout(() => {
            setConfirmLoading(false);
            setVisible(false);
            message.success('Se han actualizado los datos del usuario');
        }, 1000);
        updateData(response)
    }

    const logoutSession = () => {
        setBtnLoading(true)
        setTimeout(() => {
            message.success('Se ha cerrado la sesión');
            setBtnLoading(false)
            logout()
        }, 1000);

    }

    const getRandomColor = () => {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    useEffect(() => {
        setAvatarColor(getRandomColor())
    }, [userData && userData.email]);

    if (userData)
        return (
            <Fragment>
                <Row justify='center' align="middle" className='mt-3 px-2' style={{ backgroundColor: 'white' }}>
                    <Col>
                        <Row className='my-2'>
                            <Col >
                                <Descriptions
                                    bordered
                                    title={<Space>
                                        <Avatar style={{ backgroundColor: avatarColor, verticalAlign: 'middle' }} size="large">
                                            {userData.name[0]}
                                        </Avatar>
                                        {userData.name}
                                    </Space>}
                                    extra={<Button type="primary" onClick={showModal}>Edit</Button>}>
                                    <Descriptions.Item label='Nombre'> {userData.name}</Descriptions.Item>
                                    <Descriptions.Item label='Correo'> {userData.email}</Descriptions.Item>
                                    <Descriptions.Item label='Telefono'> {userData.phone}</Descriptions.Item>
                                    <Descriptions.Item label='País'> {userData.country}</Descriptions.Item>
                                    <Descriptions.Item label='Género'> {userData.gender}</Descriptions.Item>
                                </Descriptions>
                            </Col>
                        </Row>
                        <Row className='my-2' justify='center'>
                            <Col>
                                <Button onClick={logoutSession} size='large' danger loading={btnLoading}>Cerrar Sesión</Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Modal
                    title="Editar Usuario"
                    onCancel={handleCancel}
                    visible={visible}
                    confirmLoading={confirmLoading}
                    footer={[
                        <Button key='cancel' onClick={handleCancel}> Cancelar </Button>,
                        <Button key='submit' form='customForm' type="primary" htmlType="submit" loading={confirmLoading}> Actualizar </Button>
                    ]}
                >
                    <FormComponent fields={userFields} initialValues={userData} onClickButton={updateUser} />
                </Modal>
            </Fragment>
        )
    return (
        <Row justify='center' align="middle" style={{ minHeight: '70vh' }}>
            <Col xs={22} sm={16} md={14} lg={12} xl={10}>
                <CardLogin />
            </Col>
        </Row>
    )
}

const mapStateToProps = (state) => ({
    userData: state.userData
})

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => {
            dispatch(logoutUser())
        },
        updateData: (data) => {
            dispatch(updateUser(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent)