import logo from './logo.svg';
import './App.css';

import HomeComponent from "./components/homeComponent";
import { BrowserRouter as Router } from "react-router-dom";

import { Row, Col, Layout, Typography } from "antd";
import { LinkedinOutlined } from '@ant-design/icons';
import { blue } from '@ant-design/colors';

const { Text, Link } = Typography
const { Header, Footer, Sider, Content } = Layout;

function App() {
  return (
    <Router>
      <Layout className='main-layout'>
        <Header>
          <h1 style={{color:'white'}}>Mob-App</h1>
        </Header>
        <Content className='content'>
          <HomeComponent />
        </Content>
        <Footer>
          <Row justify='center'>
            <Col style={{textAlign:'center'}}><Text type='secondary'>Development by Eduar Sanchez</Text></Col>
          </Row>
          <Row justify='center' style={{marginTop: '.5em'}}>
            <Link href='https://www.linkedin.com/in/esteban-sanchez-ricaurte-b00040191' target='_blank'><LinkedinOutlined style={{fontSize:'2em', color: '#0A66C2'}}/></Link>
          </Row>
        </Footer>
      </Layout>
    </Router>
  );
}

export default App;
