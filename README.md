# Entorno del proyecto

Este proyecto fue desarrollado con:
- [Create React App](https://github.com/facebook/create-react-app).
- [Ant Design](https://github.com/ant-design/ant-design/).
- [Redux](https://github.com/reduxjs/redux)

## Descripción del proyecto

Se realizo el desarrollo de una aplicación de login, donde el usuario puede acceder y modificar sus datos.
Como base de datos se utilizo el archivo **"users.json"** el cual deberá ejecutarse en el puerto **"3010"** de la siguiente manera:
### `json-server --port 3010 users.json`

## Scripts Validos

En el directorio del proyecto, se pueden correr los siguientes comandos

### `yarn start`

Corre la aplicación en modo desarrollo.
Se abre [http://localhost:3000](http://localhost:3000) para visualizar el proyecto en el navegador

La pagina se actualizará si se realizan ediciones
